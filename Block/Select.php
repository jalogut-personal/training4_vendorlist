<?php
/**
 * Select.php
 *
 * @category  Training4
 * @package   Training4_VendorList
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training4\VendorList\Block;

class Select extends \Magento\Framework\View\Element\Template
{
    /**
     * @var null|\Training4\Vendor\Model\Resource\Vendor\Collection
     */
    protected $vendorCollection = null;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Training4\Vendor\Model\Resource\Vendor\Collection $collection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Training4\Vendor\Model\Resource\Vendor\Collection $collection,
        array $data = []
    ) {
        $this->vendorCollection = $collection;
        parent::__construct($context, $data);
    }

    /**
     * Gets Vendor List by name and sorting
     *
     * @return null|\Training4\Vendor\Model\Resource\Vendor\Collection
     */
    public function getVendorsList()
    {
        $sort = $this->getRequest()->getParam('sort', 'ASC');
        $search = $this->getRequest()->getParam('name');

        $this->vendorCollection->setOrder('name', $sort);

        if ($search) {
            $this->vendorCollection->addFieldToFilter('name', array('like' => '%' . $search . '%'));
        }

        return $this->vendorCollection;
    }

    /**
     * Gets form url
     *
     * @return string
     */
    public function getFormUrl()
    {
        return $this->getUrl('*/*/select');
    }
}