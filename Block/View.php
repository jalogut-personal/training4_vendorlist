<?php
/**
 * View.php
 *
 * @category  Training4
 * @package   Training4_VendorList
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training4\VendorList\Block;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Training4\Vendor\Model\Vendor
     */
    protected $vendor;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Training4\Vendor\Model\Vendor $vendor
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Training4\Vendor\Model\Vendor $vendor,
        array $data = []
    ) {
        $this->vendor = $vendor;
        parent::__construct($context, $data);
    }

    /**
     * Get Vendor
     *
     * @return \Training4\Vendor\Model\Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * Get products for Vendor
     *
     * @return mixed
     */
    public function getProductsForVendor()
    {
        if (isset($this->vendor)) {
            return $this->vendor->getProducts();
        }
        return false;
    }
}
