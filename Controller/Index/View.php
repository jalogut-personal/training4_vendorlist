<?php
/**
 * View.php
 *
 * @category  Training4
 * @package   Training4_VendorList
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training4\VendorList\Controller\Index;

class View extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Training4\Vendor\Model\Vendor
     */
    protected $vendorModel;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Training4\Vendor\Model\Vendor $vendor
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Training4\Vendor\Model\Vendor $vendor
    ) {
        $this->vendorModel = $vendor;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $vendorId = $this->getRequest()->getParam('id');
        if (!$vendorId) {
            $this->_redirect('*/*/select');
        }
        $this->vendorModel->load($vendorId);
        return $this->resultPageFactory->create();
    }
}
